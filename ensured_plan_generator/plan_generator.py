from datetime import timedelta

from ensured_plan_generator.model import Order, Plan, Spec, Store, \
    StoreTransaction

__all__ = [
    'generate_plan',
]


def generate_plan(config: dict) -> None:

    store = Store.read_from_table(config['store'])

    plan = Plan.read_from_table(config['plan'])

    spec = Spec.read_from_table(
        config['spec'],
        store.materials,
        {product.identity: product for product in plan.products}
    )

    orders = []
    earliest_transaction_date = store.earliest_transaction_date

    for order in plan:
        date = order.date

        for entry in spec[order.product]:
            date = max(
                date,
                store.calc_manufacturing_date(
                    entry.material,
                    max(order.date, earliest_transaction_date),
                    order.quantity * entry.quantity
                )
            )

        orders.append(
            Order(
                order.name,
                order.product,
                order.quantity,
                date
            )
        )

        for entry in spec[order.product]:
            store.transactions[entry.material].append(
                StoreTransaction(
                    date - timedelta(seconds=1),
                    - entry.quantity * order.quantity
                )
            )

    Plan(orders).write_data(config['output']['plan'])
    store.write_data(config['output']['store'])
