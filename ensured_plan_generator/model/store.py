from collections import defaultdict
from datetime import datetime, timedelta
from itertools import chain
from operator import attrgetter
from typing import DefaultDict, Dict, Iterator, List, Sequence, Union

from ensured_plan_generator.io import read_from_table, write_to_xlsx
from .entity import Entity

__all__ = [
    'StoreTransaction',
    'Store',
]


_ONE_DAY_TIMEDELTA = timedelta(1)


def _round_up_to_day(value: datetime) -> datetime:
    return value.replace(
        hour=0,
        minute=0,
        second=0,
        microsecond=0,
    )


class StoreTransaction(object):

    def __init__(self,
                 date: datetime,
                 quantity: Union[int, float],
                 *args,
                 **kwargs) -> None:
        super().__init__(*args, **kwargs)
        self.date = date
        self.quantity = quantity


def _update_report_row(row: Dict,
                       dates: Sequence[datetime],
                       transactions: Iterator[StoreTransaction]) -> Dict:
    for date in dates:
        row[date] = 0

    for transaction in transactions:
        date = _round_up_to_day(transaction.date)
        row[date] += transaction.quantity

    return row


class Store(object):

    def __init__(self,
                 materials: Dict[str, Entity],
                 transactions: DefaultDict[Entity, List[StoreTransaction]],
                 *args,
                 **kwargs) -> None:
        super().__init__(*args, **kwargs)
        self.materials = materials
        self.transactions = transactions

    def _get_material_balance(self,
                              material: Entity,
                              date: datetime) -> float:
        return sum(
            map(
                attrgetter('quantity'),
                filter(
                    lambda transaction: transaction.date < date,
                    self.transactions[material]
                )
            )
        )

    def _get_material_remain(self,
                             material: Entity,
                             date: datetime) -> float:
        material_balance = self._get_material_balance(material, date)
        current_date = date
        latest_transaction_date = self.latest_transaction_date

        while current_date <= latest_transaction_date:
            material_balance = min(
                material_balance,
                self._get_material_balance(
                    material,
                    current_date
                )
            )
            current_date += _ONE_DAY_TIMEDELTA
        return material_balance

    def calc_manufacturing_date(self,
                                material: Entity,
                                date: datetime,
                                quantity: float) -> datetime:
        result_date = date
        while self._get_material_remain(material, result_date) < quantity:
            result_date += _ONE_DAY_TIMEDELTA
        return result_date

    def _get_extremum_transaction_date(self,
                                       extremum_fn: callable) -> datetime:
        earliest_date = extremum_fn(map(
            attrgetter('date'),
            chain.from_iterable(self.transactions.values())
        ))
        return _round_up_to_day(earliest_date)

    @property
    def earliest_transaction_date(self) -> datetime:
        return self._get_extremum_transaction_date(min)

    @property
    def latest_transaction_date(self) -> datetime:
        return self._get_extremum_transaction_date(max)

    def write_data(self, config: dict) -> None:
        earliest_transaction_date = self.earliest_transaction_date
        report_dates = [
            earliest_transaction_date + timedelta(n)
            for n in range((
                self.latest_transaction_date - earliest_transaction_date
            ).days + 1)
        ]

        report = []
        append = report.append

        for material, transactions in self.transactions.items():
            identity = material.identity
            name = material.name

            row = {
                'IDENTITY': identity,
                'NAME': name,
            }

            for date in report_dates:
                row[date] = self._get_material_balance(material, date)

            append(row)

            row = {
                'IDENTITY': '(+) {}'.format(identity),
                'NAME': name
            }

            append(_update_report_row(
                row,
                report_dates,
                filter(
                    lambda transaction: transaction.quantity > 0,
                    transactions
                )
            ))

            row = {
                'IDENTITY': '(-) {}'.format(identity),
                'NAME': name
            }

            append(_update_report_row(
                row,
                report_dates,
                filter(
                    lambda transaction: transaction.quantity < 0,
                    transactions
                )
            ))

        write_to_xlsx(
            config,
            report
        )

    @classmethod
    def read_from_table(cls, config: dict) -> 'Store':
        data = read_from_table(config)
        materials = {}
        transactions = defaultdict(list)

        for row in data:
            identity = row['identity']

            if identity not in materials:
                materials[identity] = material = Entity(
                    identity=identity,
                    name=row['name']
                )
            else:
                material = materials[identity]

            if row['quantity'] <= 0:
                continue

            transactions[material].append(
                StoreTransaction(
                    row['date'],
                    row['quantity']
                )
            )

        return cls(materials, transactions)
