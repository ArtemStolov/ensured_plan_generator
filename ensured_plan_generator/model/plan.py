from operator import attrgetter
from typing import List, Sequence

from ensured_plan_generator.io import read_from_table, write_to_xlsx
from .entity import Entity
from .order import Order

__all__ = [
    'Plan',
]


class Plan(Sequence):

    def __init__(self,
                 orders: List[Order],
                 *args,
                 **kwargs) -> None:
        super().__init__(*args, **kwargs)
        self._orders = orders

    def __getitem__(self, key: int) -> Order:
        return self._orders[key]

    def __len__(self) -> int:
        return len(self._orders)

    @property
    def products(self):
        return frozenset(map(
            attrgetter('product'), self._orders
        ))

    def write_data(self, config: dict) -> None:

        report = [{
            'ORDER': order.name,
            'CODE': order.product.identity,
            'NAME': order.product.name,
            'AMOUNT': round(order.quantity, 3),
            'DATE': order.date
        } for order in self._orders]

        write_to_xlsx(
            config,
            report
        )

    @classmethod
    def read_from_table(cls, config: dict) -> 'Plan':
        data = read_from_table(config)
        products = {}
        orders = []

        for row in data:
            identity = row['identity']

            if identity not in products:
                products[identity] = product = Entity(
                    identity,
                    row['name']
                )

            else:
                product = products[identity]

            orders.append(
                Order(
                    row['order_name'],
                    product,
                    row['quantity'],
                    row['date']
                )
            )

        return cls(orders)
