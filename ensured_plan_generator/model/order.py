from datetime import datetime
from typing import Union

from .entity import Entity

__all__ = [
    'Order',
]


class Order(object):

    def __init__(self,
                 name: str,
                 product: Entity,
                 quantity: Union[int, float],
                 date: datetime,
                 *args,
                 **kwargs) -> None:
        super().__init__(*args, **kwargs)
        self.name = name
        self.product = product
        self.quantity = quantity
        self.date = date
