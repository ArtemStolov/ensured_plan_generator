from collections import defaultdict
from typing import Dict, Iterator, List, Mapping, Union

from ensured_plan_generator.io import read_from_table
from .entity import Entity

__all__ = [
    'SpecEntry',
    'Spec',
]


class SpecEntry(object):

    def __init__(self,
                 material: Entity,
                 quantity: Union[int, float],
                 *args,
                 **kwargs)-> None:
        super().__init__(*args, **kwargs)
        self.material = material
        self.quantity = quantity


class Spec(Mapping):

    def __init__(self,
                 entries: Dict[Entity, List[SpecEntry]],
                 *args,
                 **kwargs) -> None:
        super().__init__(*args, **kwargs)
        self._entries = entries

    def __getitem__(self, key: Entity) -> List[SpecEntry]:
        return self._entries[key]

    def __len__(self) -> int:
        return len(self._entries)

    def __iter__(self) -> Iterator[Entity]:
        return iter(self._entries)

    @classmethod
    def read_from_table(cls,
                        config: dict,
                        materials: Dict[str, Entity],
                        products: Dict[str, Entity]) -> 'Spec':
        data = read_from_table(config)
        entries = defaultdict(list)

        for row in data:
            identity = row['parent']
            if identity not in products:
                continue

            product = products[identity]
            entries[product].append(
                SpecEntry(
                    materials[row['child']],
                    row['quantity']
                )
            )

        return cls(entries)
