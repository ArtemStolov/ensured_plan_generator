__all__ = [
    'Entity',
]


class Entity(object):

    def __init__(self,
                 identity: str,
                 name: str,
                 *args,
                 **kwargs) -> None:
        super().__init__(*args, **kwargs)
        self.identity = identity
        self.name = name
