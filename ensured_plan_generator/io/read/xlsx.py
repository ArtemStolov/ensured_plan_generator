from time import time

from openpyxl import load_workbook

from ..base import Base

__all__ = [
    'XlsxReader',
]


class XlsxReader(Base):
    def __init__(
            self, 
            xlsx, 
            worksheet=None, 
            columns=None, 
            title_row=1, 
            *args, 
            **kwargs
    ):
        super().__init__(*args, **kwargs)
        self.worksheets = {}
        self.xlsx = xlsx

        if worksheet is None:
            self.read_all_worksheets(title_row=title_row)
        else:
            self.read_single_worksheet(
                worksheet=worksheet,
                columns=columns,
                title_row=title_row
            )

    def read_all_worksheets(self, title_row):
        wb = load_workbook(self.xlsx, read_only=True, data_only=True)
        for ws in wb:
            self.read_single_worksheet(
                worksheet=ws.title,
                columns=None,
                title_row=title_row
            )

    def read_single_worksheet(self, worksheet, columns, title_row):
        logger = self._logger
        t = time()
        logger.info('Начинаем считывание xlsx файла \'{}\', вкладка \'{}\''
                    .format(self.xlsx, worksheet))

        wb = load_workbook(self.xlsx, read_only=True, data_only=True)
        ws = wb[worksheet]
        title = {}

        if ws.max_row < title_row:
            logger.info('таблица пустая, считывание завершено')
            return

        self.worksheets[worksheet] = []

        for col in ws[title_row]:
            if col.value is not None:
                title[col.value] = col.column-1
                # номер колонки выдает не от 0, а от 1

        if columns is None:
            columns = {}
            for key in title:
                columns[key] = key

        if set(columns.values())-set(title.keys()):
            logger.info('Не хватает обязательных столбцов')
            logger.info(set(columns.values()) - set(title.keys()))
            return

        i = 0

        for _row in ws.rows:
            i += 1
            if i <= title_row:
                continue
            result_row = {'ROW_NUMBER': i}
            row_is_not_empty = False
            for key in columns:
                column_number = title[columns[key]]
                if _row[column_number].value is not None:
                    row_is_not_empty = True
                result_row[key] = _row[column_number].value
            if row_is_not_empty:
                self.worksheets[worksheet].append(result_row)
        logger.info('Считывание вкладки \'{}\' из файла \'{}\' '
                    'успешно завершено за {:.2f} секунд'
                    .format(worksheet, self.xlsx, time()-t))

        wb.close()
