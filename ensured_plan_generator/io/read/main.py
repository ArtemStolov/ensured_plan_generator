from logging import Logger, getLogger
from os.path import splitext
from typing import Any

from .csv import CsvReader
from .xlsx import XlsxReader

__all__ = [
    'read_from_table',
]

_logger = getLogger(__name__)


def read_from_table(config: dict, logger: Logger =_logger) -> Any:

    logger.info('Начинаем считывание файла {}, вкладка {}.'
                .format(config['path'], config['worksheet']))

    _, ext = splitext(config['path'])
    ext = ext.lower()

    logger.debug('Расширение файла распознано как {}.'.format(ext))

    if ext == '.csv':
        return CsvReader(
            config['path'],
            config['columns']
        )

    if ext == '.xlsx':
        return XlsxReader(
            config['path'],
            config['worksheet'],
            config['columns']
        ).worksheets[config['worksheet']]

    logger.error('Файлы с расширением {} не поддерживаются.'
                 .format(ext))
    raise OSError()
