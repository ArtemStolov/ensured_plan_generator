from csv import writer
from time import time
from typing import Mapping, Sequence

__all__ = [
    'write_csv',
]


def write_csv(csv_filepath: str, data: Sequence[Mapping]) -> None:
    """
    Запись словаря в csv файл
    """

    t = time()
    print("Начинаем считывание csv файла \"{}\"".format(csv_filepath))
    with open(csv_filepath, 'w', encoding='utf-8') as file:
        ws = writer(file)

        ws.writerow(key for key in data[0])
        for entry in data:
            ws.writerow(entry[key] for key in data[0])

    print("Сохранение на вкладке в файл \"{}\" успешно "
          "завершено за {:.2f} секунд".format(csv_filepath, time() - t))
