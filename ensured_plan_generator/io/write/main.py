from logging import Logger, getLogger
from typing import Any

from .xlsx import xlsx_write_data

__all__ = [
    'write_to_xlsx',
]


_logger = getLogger(__name__)


def write_to_xlsx(config: dict, data: Any, logger: Logger = _logger):
    logger.info('Начинаем запись файла {}, вкладка {}'
                .format(config['path'], config['worksheet']))

    xlsx_write_data(
        xlsx=config['path'],
        worksheet=config['worksheet'],
        overwrite=config['overwrite'],
        data=data
    )
