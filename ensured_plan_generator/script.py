from argparse import ArgumentParser
from logging import DEBUG, INFO, basicConfig
from os import getcwd
from os.path import join

from ensured_plan_generator.config import read_config
from ensured_plan_generator.plan_generator import generate_plan

__all__ = [
    'run_plan_generator',
]


def run_plan_generator():
    parser = ArgumentParser(
        description='Инструмент для генерации плана запуска, обеспеченного '
                    'комплектующими для последующего его импорта в систему IA.'
    )
    parser.add_argument('-c', '--config', required=False,
                        default=join(getcwd(), 'ensured_plan_generator.yml'))
    parser.add_argument('-d', '--debug', required=False, action='store_true',
                        default=False)

    args = parser.parse_args()

    basicConfig(
        level=args.debug and DEBUG or INFO,
        format='%(asctime)s %(levelname)-5.5s [%(name)s] %(message)s'
    )

    config = read_config(args.config)

    generate_plan(config)


if __name__ == '__main__':
    run_plan_generator()
