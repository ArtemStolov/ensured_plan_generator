from setuptools import find_packages, setup

setup(
    name='ensured_plan_generator',
    version='0.1.0',
    packages=find_packages(),
    url='https://bitbucket.org/ArtemStolov/ensured_plan_generator',
    license='MIT',
    author='BFG-Soft',
    author_email='saa@bfg-soft.ru',
    description='Ensured plan generator tool.',
    classifiers=[
        'Development Status :: 4 - Beta',
        'Environment :: Console',
        'Intended Audience :: Manufacturing',
        'Natural Language :: Russian',
        'Operating System :: OS Independent',
        'Programming Language :: Python :: 3.5',
        'Programming Language :: Python :: 3.6',
        'Topic :: Scientific/Engineering',
        'Topic :: Utilities',
    ],
    install_requires=[
        'PyYAML',
        'openpyxl',
    ],
    entry_points={
        'console_scripts': [
            'ensured_plan_generator = '
            'ensured_plan_generator.script:run_plan_generator'
        ]
    }
)
